import requests
import csv
import email, smtplib, ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from bs4 import BeautifulSoup

# PART I
url = 'https://www.ksta.de/action/ksta/4484314/search?query=Unitymedia'

rsp = requests.get(url, headers={"user-agent": "Mozilla/5.0"})

# status = rsp.status_code

raw_html = rsp.text

# print(raw_html)

soup = BeautifulSoup(raw_html, 'html.parser')
# page_title = soup.title.string

search = soup.find("article", "dm_search_result")

article_list = search.findChildren("article")

article_list_formatted = []

# print(article_list[0])
for article in article_list:
    title = article.find('h3', 'teaser_heading').text.strip()
    time = article.find('time', 'date').text
    teaser = article.find('p', 'teaser_paragraph').text
    category = article.find('div', 'teaser_category').text
    article_formatted = [category, title, time, teaser]
    article_list_formatted.append(article_formatted)

# print(article_list_formatted)

# PART II

# demoData = [['BGH Urteil', '27.05.1996', 'Der BGH entschied sich für XYZ']]
headers = ['Category', 'Title', 'Time', 'Teaser']


with open('articles.csv', 'w') as csvFile:
    writer = csv.writer(csvFile)
    writer.writerow(headers)
    writer.writerows(article_list_formatted)

csvFile.close()

# PART III

betreff = "Attention Update"
body = "This is the Update from your Script :-))"
sender_email = "mixbened@gmail.com"
empfaenger_email = "ben.bjsg92@zapiermail.com"
passwort = "Bademeister1"

message = MIMEMultipart()
message["From"] = sender_email
message["To"] = empfaenger_email
message["Subject"] = betreff

message.attach(MIMEText(body, "plain"))
datei = "articles.csv"

with open(datei, "rb") as attachment:
    part = MIMEBase('application', 'octet-stream')
    part.set_payload(attachment.read())

encoders.encode_base64(part)

part.add_header("Content-Disposition", f"attachment; filename={datei}")

message.attach(part)
text = message.as_string()

context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
    server.login(sender_email, passwort)
    server.sendmail(sender_email, empfaenger_email, text)